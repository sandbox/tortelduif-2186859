CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

 INTRODUCTION
------------
This module enables the bpost api integration for drupal commerce and shipping.

 REQUIREMENT
------------
This module depends on the following modules:
-commerce
-commerce shipping
-colorbox

 INSTALLATION
------------

Step1.
Install the module and its dependency's

Step2.
Configure the module at /admin/commerce/config/bpost-api/config

Step3.
Add the required shipping methods and configure the components at
/admin/commerce/config/shipping/methods

 CONFIGURATION
------------
Add a rule on order complete

Event: After updating an existing commerce-bestelling
Condtions: Exclude all of the methods that are NOT regular
(pugo, ...) (or only for Regular)
Actions: Create national bpost order

This will create a bpost order for regular shipping @ home.

 TROUBLESHOOTING
------------
NONE YET

 FAQ
------------
NONE YET

 MAINTAINERS
------------
This project was made by Harings Rob
Sponsored by Easycom (http://easy-webdesign.be)
