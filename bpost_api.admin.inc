<?php

/**
 * @file
 * Contains the administrative page and form callbacks for the Flat Rate module.
 */

/**
 * Builds the bpost configruation form.
 */
function bpost_api_admin_form($form, &$form_state) {

  $form = array();

  $form['bpost_api_accountid'] = array(
    '#type' => 'textfield',
    '#title' => t('Bpost AccountID'),
    '#default_value' => variable_get('bpost_api_accountid', ''),
    '#description' => t('Your bpost accountID.'),
    '#required' => TRUE,
    '#size' => 32,
    '#maxlength' => 255,
  );

  $form['bpost_api_passphrase'] = array(
    '#type' => 'textfield',
    '#title' => t('Bpost Passphrase'),
    '#default_value' => variable_get('bpost_api_passphrase', ''),
    '#description' => t('Your bpost passphrase.'),
    '#required' => TRUE,
    '#size' => 32,
    '#maxlength' => 255,
  );

  $form['bpost_api_signedpackage'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable signatures on package delivery'),
    '#default_value' => variable_get('bpost_api_signedpackage', '0'),
    '#required' => TRUE,
    '#size' => 32,
    '#maxlength' => 255,
  );

  $form['bpost_api_signedpackage_amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Set the minimum amount required for a signature (option above). 0 for always'),
    '#default_value' => variable_get('bpost_api_signedpackage_amount', '0'),
    '#required' => TRUE,
    '#size' => 32,
    '#maxlength' => 255,
  );

  $form['bpost_api_sender_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Sender name'),
    '#default_value' => variable_get('bpost_api_sender_name', ''),
    '#required' => TRUE,
    '#size' => 32,
    '#maxlength' => 255,
  );

  $form['bpost_api_sender_street'] = array(
    '#type' => 'textfield',
    '#title' => t('Street name'),
    '#default_value' => variable_get('bpost_api_sender_street', ''),
    '#required' => TRUE,
    '#size' => 32,
    '#maxlength' => 255,
  );

  $form['bpost_api_sender_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Housenumber'),
    '#default_value' => variable_get('bpost_api_sender_number', ''),
    '#required' => TRUE,
    '#size' => 32,
    '#maxlength' => 255,
  );

  $form['bpost_api_sender_postalcode'] = array(
    '#type' => 'textfield',
    '#title' => t('Postal Code'),
    '#default_value' => variable_get('bpost_api_sender_postalcode', ''),
    '#required' => TRUE,
    '#size' => 32,
    '#maxlength' => 255,
  );

  $form['bpost_api_sender_locality'] = array(
    '#type' => 'textfield',
    '#title' => t('Locality'),
    '#default_value' => variable_get('bpost_api_sender_locality', ''),
    '#required' => TRUE,
    '#size' => 32,
    '#maxlength' => 255,
  );

  $form['bpost_api_sender_countrycode'] = array(
    '#type' => 'textfield',
    '#title' => t('Countrycode'),
    '#default_value' => variable_get('bpost_api_sender_countrycode', 'BE'),
    '#required' => TRUE,
    '#size' => 32,
    '#maxlength' => 255,
  );

  $form['bpost_api_sender_email'] = array(
    '#type' => 'textfield',
    '#title' => t('E-mail'),
    '#default_value' => variable_get('bpost_api_sender_email', ''),
    '#required' => TRUE,
    '#size' => 32,
    '#maxlength' => 255,
  );

  $form['bpost_api_sender_phone'] = array(
    '#type' => 'textfield',
    '#title' => t('Phone number'),
    '#default_value' => variable_get('bpost_api_sender_phone', ''),
    '#required' => TRUE,
    '#size' => 32,
    '#maxlength' => 255,
  );

  return system_settings_form($form);
}


/**
 * Builds the form for adding and editing flat rate services.
 */
function bpost_api_service_form($form, &$form_state, $shipping_service) {
  // Store the initial shipping service in the form state.
  $form_state['shipping_service'] = $shipping_service;

  $form['bpost_rate'] = array(
    '#tree' => TRUE,
  );

  $form['bpost_rate']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $shipping_service['title'],
    '#description' => t('The administrative title of this flat rate. It is recommended that this title begin with a capital letter and contain only letters, numbers, and spaces.'),
    '#required' => TRUE,
    '#size' => 32,
    '#maxlength' => 255,
    '#field_suffix' => ' <small id="edit-flat-rate-title-suffix">' . t('Machine name: @name', array('@name' => $shipping_service['name'])) . '</small>',
  );

  if (empty($shipping_service['name'])) {
    $form['bpost_rate']['name'] = array(
      '#type' => 'machine_name',
      '#title' => t('Machine name'),
      '#default_value' => $shipping_service['name'],
      '#maxlength' => 32,
      '#required' => TRUE,
      '#machine_name' => array(
        'exists' => 'commerce_shipping_service_load',
        'source' => array('bpost_rate', 'title'),
      ),
      '#description' => t('The machine-name of this flat rate. This name must contain only lowercase letters, numbers, and underscores. It must be unique.'),
    );
  }
  else {
    $form['bpost_rate']['name'] = array(
      '#type' => 'value',
      '#value' => $shipping_service['name'],
    );
  }

  $form['bpost_rate']['display_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Display title'),
    '#default_value' => $shipping_service['display_title'],
    '#description' => t('The front end display title of this flat rate shown to customers. Leave blank to default to the <em>Title</em> from above.'),
    '#size' => 32,
  );

  $form['bpost_rate']['description'] = array(
    '#type' => 'select',
    '#title' => t('Type'),
    '#description' => t('Fill in the shipping service type: <strong>parcels_depot</strong> => bpack 24/7 | <strong>pugo</strong> => bpack@bpost | <strong>regular</strong> => bpack@home | <strong>pickup</strong> => The customer can select this option to enable the customer to pickup the product at the store address'),
    '#default_value' => $shipping_service['description'],
    '#options' => array(
      'regular' => t('Bpack@home'),
      'pugo' => t('bpack@bpost'),
      'parcels_depot' => t('bpack 24/7'),
      'pickup' => t('Pickup at store address'),
    ),
  );

  $form['bpost_rate']['amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Base rate'),
    '#default_value' => commerce_currency_amount_to_decimal($shipping_service['base_rate']['amount'], $shipping_service['base_rate']['currency_code']),
    '#required' => TRUE,
    '#size' => 10,
    '#prefix' => '<div class="commerce-flat-rate-base-rate">',
  );

  // Build a currency options list from all enabled currencies.
  $options = commerce_currency_get_code(TRUE);

  // If the current currency value is not available, add it now with a
  // message in the help text explaining it.
  if (empty($options[$shipping_service['base_rate']['currency_code']])) {
    $options[$shipping_service['base_rate']['currency_code']] = check_plain($shipping_service['base_rate']['currency_code']);

    $description = t('The currency set for this rate is not currently enabled. If you change it now, you will not be able to set it back.');
  }
  else {
    $description = '';
  }

  // If only one currency option is available, don't use a select list.
  if (count($options) == 1) {
    $currency_code = key($options);

    $form['bpost_rate']['amount']['#field_suffix'] = check_plain($currency_code);
    $form['bpost_rate']['amount']['#suffix'] = '</div>';

    $form['bpost_rate']['currency_code'] = array(
      '#type' => 'value',
      '#default_value' => $currency_code,
    );
  }
  else {
    $form['bpost_rate']['currency_code'] = array(
      '#type' => 'select',
      '#description' => $description,
      '#options' => $options,
      '#default_value' => $shipping_service['base_rate']['currency_code'],
      '#suffix' => '</div>',
    );
  }

  // Add support for base rates including tax.
  if (module_exists('commerce_tax')) {
    // Build an array of tax types that are display inclusive.
    $inclusive_types = array();

    foreach (commerce_tax_types() as $name => $tax_type) {
      if ($tax_type['display_inclusive']) {
        $inclusive_types[$name] = $tax_type['title'];
      }
    }

    // Build an options array of tax rates of these types.
    $options = array();

    foreach (commerce_tax_rates() as $name => $tax_rate) {
      if (in_array($tax_rate['type'], array_keys($inclusive_types))) {
        $options[$inclusive_types[$tax_rate['type']]][$name] = t('Including @title', array('@title' => $tax_rate['title']));
      }
    }

    if (!empty($options)) {
      // Find the default value for the tax included element.
      $default = '';

      if (!empty($shipping_service['data']['include_tax'])) {
        $default = $shipping_service['data']['include_tax'];
      }

      $form['bpost_rate']['currency_code']['#title'] = '&nbsp;';

      $form['bpost_rate']['include_tax'] = array(
        '#type' => 'select',
        '#title' => t('Include tax in this rate'),
        '#description' => t('Saving a rate tax inclusive will bypass later calculations for the specified tax.'),
        '#options' => count($options) == 1 ? reset($options) : $options,
        '#default_value' => $default,
        '#required' => FALSE,
        '#empty_value' => '',
      );
    }
  }

  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 40,
  );

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Bpost rate'),
  );

  if (!empty($form_state['shipping_service']['name'])) {
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete Bpost rate'),
      '#suffix' => l(t('Cancel'), 'admin/commerce/config/shipping/services/bpost-api-shipping-method'),
      '#submit' => array('bpost_api_service_delete_form_submit'),
      '#weight' => 45,
    );
  }
  else {
    $form['actions']['submit']['#suffix'] = l(t('Cancel'), 'admin/commerce/config/shipping/services/bpost-api-shipping-method');
  }

  return $form;
}

/**
 * Validate handler: ensures a valid base rate was entered for the flat rate.
 */
function bpost_api_service_form_validate($form, &$form_state) {
  // Ensure the rate amount is numeric.
  if (!is_numeric($form_state['values']['bpost_rate']['amount'])) {
    form_set_error('bpost_rate][amount', t('You must enter a numeric value for the base rate amount.'));
  }
  else {
    // Convert the decimal amount value entered to an
    // integer based amount value.
    form_set_value($form['bpost_rate']['amount'], commerce_currency_decimal_to_amount($form_state['values']['bpost_rate']['amount'], $form_state['values']['bpost_rate']['currency_code']), $form_state);
  }
}

/**
 * Submit handler: saves the new or updated flat rate service.
 */
function bpost_api_service_form_submit($form, &$form_state) {
  $shipping_service = $form_state['shipping_service'];

  $looparray = array(
    'name',
    'title',
    'display_title',
    'description',
    'amount',
    'currency_code',
  );

  // Update the shipping service array with values from the form.
  foreach ($looparray as $key) {
    $shipping_service[$key] = $form_state['values']['bpost_rate'][$key];
  }

  // If a tax was specified for inclusion, add it to the data array.
  if (!empty($form_state['values']['bpost_rate']['include_tax'])) {
    $shipping_service['data']['include_tax'] = $form_state['values']['bpost_rate']['include_tax'];
  }
  elseif (!empty($shipping_service['data']['include_tax'])) {
    unset($shipping_service['data']['include_tax']);
  }

  // Save the shipping service.
  unset($shipping_service['base_rate']);
  $op = bpost_api_service_save($shipping_service);

  if (!$op) {
    drupal_set_message(t('The flat rate service failed to save properly. Please review the form and try again.'), 'error');
    $form_state['rebuild'] = TRUE;
  }
  else {
    drupal_set_message(t('Flat rate service saved.'));
    $form_state['redirect'] = 'admin/commerce/config/shipping/services/bpost-api-shipping-method';
  }
}

/**
 * Submit handler: redirects to the flat rate service delete confirmation form.
 */
function bpost_api_service_form_delete_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/commerce/config/shipping/services/bpost-rate-' . strtr($form_state['shipping_service']['name'], '_', '-') . '/delete';
}

/**
 * Displays the edit form for an existing flat rate service.
 */
function bpost_api_service_edit_page($name) {
  return drupal_get_form('bpost_api_service_form', commerce_shipping_service_load($name));
}

/**
 * Builds the form for deleting flat rate services.
 */
function bpost_api_service_delete_form($form, &$form_state, $shipping_service) {
  $form_state['shipping_service'] = $shipping_service;

  $form = confirm_form($form,
    t('Are you sure you want to delete the <em>%title</em> flat rate service?', array('%title' => $shipping_service['title'])),
    'admin/commerce/config/shipping/services/bpost-api-shipping-method',
    '<p>' . t('This action cannot be undone.') . '</p>',
    t('Delete'),
    t('Cancel'),
    'confirm'
  );

  return $form;
}

/**
 * Submit callback for commerce_bpost_rate_service_delete_form().
 */
function bpost_api_service_delete_form_submit($form, &$form_state) {
  $shipping_service = $form_state['shipping_service'];

  bpost_api_service_delete($shipping_service['name']);

  drupal_set_message(t('The flat rate service <em>%title</em> has been deleted.', array('%title' => $shipping_service['title'])));
  watchdog('commerce_bpost_rate', 'Deleted flat rate service <em>%title</em>.', array('%title' => $shipping_service['title']), WATCHDOG_NOTICE);

  $form_state['redirect'] = 'admin/commerce/config/shipping/services/bpost-api-shipping-method';
}

/**
 * Displays the delete confirmation form for an existing flat rate service.
 */
function bpost_api_service_delete_page($name) {
  return drupal_get_form('bpost_api_service_delete_form', commerce_shipping_service_load($name));
}
