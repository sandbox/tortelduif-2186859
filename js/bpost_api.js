(function ($) {
  function bpostapi(type) {
    // Check if we should even load the form
    if(jQuery.inArray(type, Drupal.settings.bpostapi.dataarray) != -1){
        $.colorbox({
          width: "90%",
          reposition: false,
          scrolling: false,
          iframe: false,
          height: "90%",
          html: '<div id="bpostForm"></div><iframe width="100%" height="100%" id="openinpopup" name="my_iframe" src="/checkout/' + Drupal.settings.bpostapi.orderid + '/' + type + '/bpost-form"></iframe>'
        });
    }
  }
  Drupal.behaviors.bpost_api = {
    attach: function(context, settings) {
      var array = $.map(context, function(value, index) {
          return [value];
      });
      console.log(array[0]['id']);
      if(array[0]['id'] == "commerce-shipping-service-ajax-wrapper" || array[0]['id'] == "commerce-shipping-service-details") {
        $.colorbox.remove();
        var myval = $(".commerce_shipping .form-radio:checked").val();
        $('fieldset.bpost_api').hide();
        bpostapi(myval);
      }
    }
  }
})(jQuery);
function closeColorBox2() { jQuery('#cboxClose').click(); }
